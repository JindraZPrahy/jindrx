import type { Plugin } from "vite";
import { readdir, writeFile, readFile } from "fs/promises";
import {
  type PostMetadata,
  emptyPostMetadata,
  singleMetadata,
  arrayMetadata,
} from "./content/types";
import { output_name, path_output } from "./content/content";

let path_articles: string = "./static/articles/";

export const postMetadata: Plugin = {
  name: "Generate Post Metadata",
  async buildStart() {
    console.log("Building post metadata...");
    let posts_list = await readdir(path_articles);
    //console.log(posts_list);
    //Extract tags for each post
    let metadata_list: { [key: string]: PostMetadata } = {};
    for (let i = 0; i < posts_list.length; i++) {
      let current_post: string = posts_list[i].slice(0, -3); //Get just the short name
      let content: string = await readFile(
        path_articles + current_post + ".md",
        "utf-8"
      );
      let initial_comment: string = content!
        .match(/---([\s\S]*?)---/)![1]
        .trim(); //I am responsible for formatting post metadata

      const lines = initial_comment.split("\n");
      let metadata: PostMetadata = { ...emptyPostMetadata };
      lines.forEach((line) => {
        const index = line.indexOf(":");
        const key = line.substring(0, index).trim();
        const value = line.substring(index + 1).trim();
        if (metadata.hasOwnProperty(key)) {
          if (singleMetadata.includes(key)) {
            metadata[key] = value;
          } else if (arrayMetadata.includes(key)) {
            metadata[key] = value.split(",").map((item) => item.trim());
          }
        } else {
          throw new Error("Unknown post metadata.");
        }
      });
      if (false) {
        //Debugging
        console.log(content);
        console.log("#########################");
        console.log(initial_comment);
        console.log("#########################");
        console.log(metadata);
        break;
      }
      if (!metadata.hasOwnProperty("title")) {
        throw new Error("Each article must have a title.");
      }

      metadata_list[current_post] = metadata;
      console.log(i, metadata_list);
    }
    writeFile(
      path_output + output_name,
      JSON.stringify(metadata_list, null, 2)
    );
    console.log("Article metadata generated.");
  },
};
