import type { Plugin } from 'svelte-exmarkdown';

//Thanks, https://github.com/m93a/digital-garden :3

//
// GFM
// Github-flavoured markdown. Useful for e. g. footnotes.

import { gfmPlugin } from 'svelte-exmarkdown/gfm';

export const gfm = gfmPlugin();

//
// Math Plugin

import remarkMath from 'remark-math';
import rehypeKatex from 'rehype-katex';

export const math: Plugin = {
	remarkPlugin: remarkMath,
	rehypePlugin: rehypeKatex,
};


//
// Raw HTML Plugin
// Allows raw html to be in the md. E. g. <!-- comments -->

import { unified } from 'unified';
import { toHtml } from 'hast-util-to-html';
import rehypeParse from 'rehype-parse';

export const rawHtml: Plugin = {
	rehypePlugin: () => (node) => {
		const str = toHtml(node, { allowDangerousHtml: true });
		return unified().use(rehypeParse, { fragment: true }).parse(str);
	},
};