export interface PostMetadata {
    title: string;
    lang: string;
    tags: string[];
    links: string[];
    notes: string;
}

export const singleMetadata = [ "title", "lang", "notes" ]
export const arrayMetadata = ["tags", "links"]
export const emptyPostMetadata: PostMetadata = {
    title: '',
    lang: '',
    tags: [],
    links: [],
    notes: '',
  };

export interface MetadataList {
    [post_id: string]: PostMetadata;
}

export interface PostFull {
    metadata: PostMetadata;
    content: string;
}

export interface tagsToPostKeys {
    //The strings are the post keys
    [tag: string]: string[];
}