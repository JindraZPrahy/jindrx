import type { MetadataList } from "./types.ts";

export function t(str: TemplateStringsArray) {
  let result = "";

  for (let i = 0; i < str.length; i++) {
    result += str[i];
  }

  result = result
    .split("\n")
    .map((row) => row.trim())
    .join("\n");

  if (result.startsWith("\n")) {
    result = result.slice(1);
  }

  result = result
    .replace(/^\s*[\r\n]/gm, "XXXX") //replace empty lines with XXXX
    .replace(/(.)\n/g, "$1 ")
    .replace(/\n/g, "") //replace \n with a space
    .replace(/\.\s($|\n)/g, ".$1") //\. \n with \.\n
    .replace(/XXXX/g, "\n\n"); //replace XXXX with two empty lines

  //console.log(result);

  return result;
}


export let output_name: string = "posts_metadata_generated.json";
export let path_output: string = "./static/";

export async function fetchPostsMetadata(fetch_fun: typeof globalThis.fetch = fetch, running: boolean=false): Promise<MetadataList> {
  try {
    let path: string;
    if (!running) {
      path = path_output + output_name;
    } else {
      path = "/" + output_name;
    }
    const response = await fetch_fun(path);
    let result = await response.json();
    return result;
  } catch (error) {
    console.error('Error fetching data:', error);
    return {}
  }
}

export let articles = "";