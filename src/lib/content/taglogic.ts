import type { tagsToPostKeys } from "./types";
import { fetchPostsMetadata } from "./content";


export async function fetchTagsToKeys(
  fetch_fun: typeof globalThis.fetch = fetch,
  running: boolean = false
): Promise<{ tagsToKeys: tagsToPostKeys, tags: string[] } > {
  let postsMetadata = await fetchPostsMetadata(fetch_fun, running);
  
  //Getting all unique tags
  let keys = Object.keys(postsMetadata);
  let tags_with_test: string[] = [];
  for (let i = 0; i < keys.length; i++) {
    let keytags = postsMetadata[keys[i]].tags;
    keytags = keytags.filter((element) => !tags_with_test.includes(element));
    tags_with_test.push(...keytags);
  }

  let tags = tags_with_test.filter((element) => !element.startsWith("__"));
  let tagsToKeys: tagsToPostKeys = {};
  console.log(tagsToKeys);
  for (let i = 0; i < tags.length; i++) {
    //console.log("tag: " + String(tags[i]));
    //For each tag, get the key of each article that has the tag
    let filteredArticles = Object.entries(postsMetadata)
      .filter(([postKey, post]) => post.tags.includes(tags[i]))
      .map(([postKey]) => postKey);
  
    tagsToKeys[tags[i]] = filteredArticles;
    //console.log(filteredArticles);
  }

  return { tagsToKeys: tagsToKeys,  tags: tags };
}
