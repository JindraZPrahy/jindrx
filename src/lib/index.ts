import { error } from "@sveltejs/kit";
import type { PostFull } from "./content/types";
//import { articles } from "$lib/content/content";

function safePath(path: string) {
  return path
    .split("/")
    .filter((s) => !["", ".", ".."].includes(s))
    .map(encodeURIComponent)
    .join("/");
}

function removeFrontMatter(str: string): string {
  str = str.replace(/---([\s\S]*?)---/, '');
  return str;
}

export function truncatemdString(inputString: string, maxLength: number = 250): string {
  inputString = removeFrontMatter(inputString);
  if (inputString.length <= maxLength) {
      return inputString;
  }

  // Find the last space within the first 250 characters
  const lastSpaceIndex = inputString.lastIndexOf(" ", maxLength);

  // If a space is found, truncate at the last space; otherwise, truncate at the maxLength
  const truncatedString = lastSpaceIndex !== -1 ? inputString.slice(0, lastSpaceIndex) : inputString.slice(0, maxLength);

  return truncatedString;
}

export async function fetchPostContent(
  fetch: typeof globalThis.fetch,
  article: string,
  truncate: boolean = false
): Promise<string> {
  article = safePath(article);
  let res: Response | undefined;

  let path = "/articles/" + article;
  if (article) res = await fetch(path + ".md");

  if (!res?.ok) throw error(404);

  let source = await res.text();
  source = removeFrontMatter(source);
  if (truncate) {
    source = truncatemdString(source);
  }
  path += ".md";

  return source;
}


//TODO: uses old way of loading post metadata
//export async function fetchPost(
//  fetch: typeof globalThis.fetch,
//  article: string,
//  truncate: boolean = false
//): Promise<PostFull> {
//  let content = await fetchPostContent(fetch, article);
//  if (truncate) {
//    content = truncatemdString(content);
//  }
//  let metadata = articles[article];
//
//  return { metadata, content };
//}
