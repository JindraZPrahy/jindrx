import { fetchTagsToKeys } from "$lib/content/taglogic.js";

export const load = async ({ fetch, params }) => {

    let both = await fetchTagsToKeys(fetch, true);
    return { tags: both.tags };
}