import { error } from "@sveltejs/kit";
import type { PostFull } from "$lib/content/types";
import { fetchPostContent } from "$lib/index";
import { fetchTagsToKeys } from "$lib/content/taglogic.js";
import { fetchPostsMetadata } from "$lib/content/content.js";

export const load = async ({ fetch, params }) => {
  let both = await fetchTagsToKeys(fetch, true);

  let tagsToKeys = both.tagsToKeys;
  let tags = both.tags;

  console.log("hiii")
  console.log(tagsToKeys)
  console.log(params)
  if (tags.includes(params.tag)) {
    let postsMetadata = await fetchPostsMetadata(fetch, true);

    let tag = params.tag;
    let postKeysForTag = tagsToKeys[tag];
    console.log(postKeysForTag)
    let postsForTag: PostFull[] = [];
    for (let i=0; i<postKeysForTag.length; i++) {
        //Assembling the post
        let postContent = await fetchPostContent(fetch, postKeysForTag[i], true);
        let postmetadata = postsMetadata[postKeysForTag[i]];
        let post = { metadata: postmetadata, content: postContent }
        postsForTag.push(post);
    }

    return { tag , postKeysForTag , postsForTag };
  } else {
    error(404, "Tag not found");
  }
};
