import { redirect } from '@sveltejs/kit';

export function load({ params }) {
    redirect(302, '/rhizome_world/')
}
