import { error } from "@sveltejs/kit";
import { fetchPostsMetadata } from "$lib/content/content";
import  { fetchPostContent } from '$lib/index';

export const load = async ({ fetch, params }) => {
  let posts = await fetchPostsMetadata(fetch, true)

  if (params.post_id in posts) {
    let content = await fetchPostContent(fetch, params.post_id);
    let metadata = posts[params.post_id];
    console.log(content);
    return { metadata: metadata, content: content };
  } else {
    error(404, "Page not found");
  }
};
