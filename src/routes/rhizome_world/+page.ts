import { fetchPostsMetadata } from "$lib/content/content";


export const load = async ({ fetch, params }) => {
    let articles = await fetchPostsMetadata(fetch, true)

    return articles;
  };
  