
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';

export const myCustomTheme: CustomThemeConfig = {
    name: 'my-custom-theme',
    properties: {
		// =~= Theme Properties =~=
		"--theme-font-family-base": `system-ui`,
		"--theme-font-family-heading": `system-ui`,
		"--theme-font-color-base": "0 0 0",
		"--theme-font-color-dark": "255 255 255",
		"--theme-rounded-base": "9999px",
		"--theme-rounded-container": "8px",
		"--theme-border-base": "1px",
		// =~= Theme On-X Colors =~=
		"--on-primary": "0 0 0",
		"--on-secondary": "255 255 255",
		"--on-tertiary": "255 255 255",
		"--on-success": "0 0 0",
		"--on-warning": "255 255 255",
		"--on-error": "255 255 255",
		"--on-surface": "255 255 255",
		// =~= Theme Colors  =~=
		// primary | #ebcffa 
		"--color-primary-50": "252 248 254", // #fcf8fe
		"--color-primary-100": "251 245 254", // #fbf5fe
		"--color-primary-200": "250 243 254", // #faf3fe
		"--color-primary-300": "247 236 253", // #f7ecfd
		"--color-primary-400": "241 221 252", // #f1ddfc
		"--color-primary-500": "235 207 250", // #ebcffa
		"--color-primary-600": "212 186 225", // #d4bae1
		"--color-primary-700": "176 155 188", // #b09bbc
		"--color-primary-800": "141 124 150", // #8d7c96
		"--color-primary-900": "115 101 123", // #73657b
		// secondary | #310afb 
		"--color-secondary-50": "224 218 254", // #e0dafe
		"--color-secondary-100": "214 206 254", // #d6cefe
		"--color-secondary-200": "204 194 254", // #ccc2fe
		"--color-secondary-300": "173 157 253", // #ad9dfd
		"--color-secondary-400": "111 84 252", // #6f54fc
		"--color-secondary-500": "49 10 251", // #310afb
		"--color-secondary-600": "44 9 226", // #2c09e2
		"--color-secondary-700": "37 8 188", // #2508bc
		"--color-secondary-800": "29 6 151", // #1d0697
		"--color-secondary-900": "24 5 123", // #18057b
		// tertiary | #952600 
		"--color-tertiary-50": "239 222 217", // #efded9
		"--color-tertiary-100": "234 212 204", // #ead4cc
		"--color-tertiary-200": "229 201 191", // #e5c9bf
		"--color-tertiary-300": "213 168 153", // #d5a899
		"--color-tertiary-400": "181 103 77", // #b5674d
		"--color-tertiary-500": "149 38 0", // #952600
		"--color-tertiary-600": "134 34 0", // #862200
		"--color-tertiary-700": "112 29 0", // #701d00
		"--color-tertiary-800": "89 23 0", // #591700
		"--color-tertiary-900": "73 19 0", // #491300
		// success | #d275e9 
		"--color-success-50": "248 234 252", // #f8eafc
		"--color-success-100": "246 227 251", // #f6e3fb
		"--color-success-200": "244 221 250", // #f4ddfa
		"--color-success-300": "237 200 246", // #edc8f6
		"--color-success-400": "224 158 240", // #e09ef0
		"--color-success-500": "210 117 233", // #d275e9
		"--color-success-600": "189 105 210", // #bd69d2
		"--color-success-700": "158 88 175", // #9e58af
		"--color-success-800": "126 70 140", // #7e468c
		"--color-success-900": "103 57 114", // #673972
		// warning | #3218a1 
		"--color-warning-50": "224 220 241", // #e0dcf1
		"--color-warning-100": "214 209 236", // #d6d1ec
		"--color-warning-200": "204 197 232", // #ccc5e8
		"--color-warning-300": "173 163 217", // #ada3d9
		"--color-warning-400": "112 93 189", // #705dbd
		"--color-warning-500": "50 24 161", // #3218a1
		"--color-warning-600": "45 22 145", // #2d1691
		"--color-warning-700": "38 18 121", // #261279
		"--color-warning-800": "30 14 97", // #1e0e61
		"--color-warning-900": "25 12 79", // #190c4f
		// error | #09871f 
		"--color-error-50": "218 237 221", // #daeddd
		"--color-error-100": "206 231 210", // #cee7d2
		"--color-error-200": "194 225 199", // #c2e1c7
		"--color-error-300": "157 207 165", // #9dcfa5
		"--color-error-400": "83 171 98", // #53ab62
		"--color-error-500": "9 135 31", // #09871f
		"--color-error-600": "8 122 28", // #087a1c
		"--color-error-700": "7 101 23", // #076517
		"--color-error-800": "5 81 19", // #055113
		"--color-error-900": "4 66 15", // #04420f
		// surface | #4a3871 
		"--color-surface-50": "228 225 234", // #e4e1ea
		"--color-surface-100": "219 215 227", // #dbd7e3
		"--color-surface-200": "210 205 220", // #d2cddc
		"--color-surface-300": "183 175 198", // #b7afc6
		"--color-surface-400": "128 116 156", // #80749c
		"--color-surface-500": "74 56 113", // #4a3871
		"--color-surface-600": "67 50 102", // #433266
		"--color-surface-700": "56 42 85", // #382a55
		"--color-surface-800": "44 34 68", // #2c2244
		"--color-surface-900": "36 27 55", // #241b37
		
	}
}