---
title: Why I started thinking about Hans Asperger too much
tags: Hans Asperger, autism, neurodivergence, 2024
notes: This is what I've been thinking about and maybe I will really go through and continue this with new posts. Maybe not 😼
---

One day, the following thoughts entered my head and they have been there ever since:

> I'm pretty sure I'm autistic. But I don't have a medical diagnosis for autism. But what
> is a diagnosis anyway? What is autism in the medical sense anyway?

That's a pretty large thing to unpack. So let's focus on one aspect of this:
there exists a concept called *Asperger's syndrome* (from now on: AS), which,
according to some is an actual *neurodevelopmental disorder*, some would maybe say an *illness*
that someone can have. For example, it's in DSM IV, but not in DSM V.

From what I've heard AS is the “high-IQ” autism. Since I find the concept of IQ harmful and dubious, this automatically makes me suspicious of AS as well. And also Hans Asperger (from here on: HA),
I am pretty sure I've heard he had some nazi involvement?

To be clear: these are my initial thoughts. I'd like to back it up with research.
So here's the two big types of questions that I would like to research[^1]
1. Is the concept of AS meaningful and is it harmful?
2. Who was HA and did he do some “problematic” things? Because if AS is a helpful concept, maybe the name should be changed. And if HA somehow did bad things, maybe his bad influence “tainted” the concept of AS.

Let's start with question 2, because why not. Here are the questions that I think would be useful
to answer.
1. Who was HA in broad strokes?
2. What was the relationship between HA and autistic people?
3. Was he, like, a nazi?

Maybe I will even discover new questions in the process.


[^1]: probably not original research, just synthesis and interpretation of what I find on the internet.