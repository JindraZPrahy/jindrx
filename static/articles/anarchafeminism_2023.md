---
title: About anarchafeminism
tags: anarchism, feminism, 2023
lang: en
notes: Why not give yet another definition of anarchism?
---

An anarchist society would be based on mutual aid, not on competition.
It would be a constantly evolving society, always trying to improve
everyone's freedom. Because freedom is a property of the whole society,
not of individuals.

Why feminism? Because one kind of oppressive hierarchy is **patriarchy**.
And sadly, a lot of anarchist groups are reinforcing it, not fighting
against it (from my experience). So I think it's worth stating explicitly.

Also, I have read some feminist literature and I considered it convincing.
Although I still don't know if I can use those ideas in practice.

Yes, I still don't know how to *be an anarchist* in my daily life.