---
title: Why write about my worldview?
lang: en
tags: meta, worldview, 2023
notes:
---

1. To formulate my thoughts more clearly.
2. To be able to see how they evolve in time.
3. Some of my worldviews are very niche, yet I am passionate about them.
I believe there is value in simply articulating them.

Still, writing my thoughts *out in the open* feels a bit awkward.
So let me say this: I wish for none of my beliefs to be unassailable.
They can and will change. And if I write them, maybe that will facilitate the change.