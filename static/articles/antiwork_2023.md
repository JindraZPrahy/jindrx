---
title: My stance on anti-work
lang: en
links: https://theanarchistlibrary.org/library/bob-black-the-abolition-of-work
tags: anti-work, worldview, anarchism, 2023
notes: This seems like an obvious moral stance to me. But perhaps the bigger question is, what is coercion and how would a world without work look like?
---

By work I mean "wage labour", i. e. doing stuff
out of coercion that you would not do otherwise. I am against that.

In our society, almost everyone works. The main method of coercion
is money: if you don't work, you don't get money and you starve. Another
method of coercion is social: people without work are somehow viewed as
"lesser". Perhaps they are called "lazy", perhaps other things. And on the
other hand: if a person works well and consistently, that is considered to
be virtuous and desireable.

I don't consider work virtuous. True: some people achieve amazing stuff as wage
labourers, perhaps they are programmers or artists and they build something amazing.

But **their creation** is amazing, not the fact that they are working. In fact,
imo they would be able to do better without coercion.
So I support anti-work, i. e. the struggle to abolish work and the wage labour system.