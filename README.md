# jindrx.space

Welcome to my house!

Maybe you're here, because you're curious about how I made this site.
It was done with Sveltekit and Skeleton (the Svelte UI library).

- [Svelte tutorial](https://learn.svelte.dev/)
- [Skeleton docs](https://www.skeleton.dev/docs/get-started)
- [A Youtube vid that introduces Skeleton](https://www.youtube.com/watch?v=P_A0qQ7AuK8&pp=ygUOc3ZlbHRlIHNrZWx0b24%3D)
